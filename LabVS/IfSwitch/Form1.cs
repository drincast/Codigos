﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IfSwitch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private bool VerSwitch(int v)
        {
            bool res = false;

            switch (v)
            {
                case 1:
                    res = true;
                    break;
                case 2:
                    res = true;
                    break;
                case 3:
                    res = true;
                    break;
                case 4:
                    res = true;
                    break;
                case 5:
                    res = true;
                    break;
                case 6:
                    res = true;
                    break;
                case 7:
                    res = true;
                    break;
                case 8:
                    res = true;
                    break;
                case 9:
                    res = true;
                    break;
                case 10:
                    res = true;
                    break;
                default:
                    res = false;
                    break;
            }
            return res;
        }

        private bool VerIf(int v)
        {
            bool res = false;

            if (v == 1)
            {
                res = true;
            }
            if (v == 2)
            {
                res = true;
            }
            if (v == 3)
            {
                res = true;
            }
            if (v == 4)
            {
                res = true;
            }
            if (v == 5)
            {
                res = true;
            }
            if (v == 6)
            {
                res = true;
            }
            if (v == 7)
            {
                res = true;
            }
            if (v == 8)
            {
                res = true;
            }
            if (v == 9)
            {
                res = true;
            }
            if (v == 10)
            {
                res = true;
            }

            return res;
        }

        private bool VerIfElse(int v)
        {
            bool res = false;

            if (v == 1)
            {
                res = true;
            }
            else
            {
                if (v == 2)
                {
                    res = true;
                }
                else
                {
                    if (v == 3)
                    {
                        res = true;
                    }
                    else
                    {
                        if (v == 4)
                        {
                            res = true;
                        }
                        else
                        {
                            if (v == 5)
                            {
                                res = true;
                            }
                            else
                            {
                                if (v == 6)
                                {
                                    res = true;
                                }
                                else
                                {
                                    if (v == 7)
                                    {
                                        res = true;
                                    }
                                    else
                                    {
                                        if (v == 8)
                                        {
                                            res = true;
                                        }
                                        else
                                        {
                                            if (v == 9)
                                            {
                                                res = true;
                                            }
                                            else
                                            {
                                                if (v == 10)
                                                {
                                                    res = true;
                                                }
                                            }                                            
                                        }                                        
                                    }                                    
                                }                                
                            }                            
                        }                        
                    }                    
                }               
            }
            return res;
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            int iter = 0, val = 0;
            Stopwatch tSwitch = null, tIf = null, tIfElse = null;

            try
            {
                iter = Convert.ToInt32(txtIter.Text);
                val = Convert.ToInt32(txtValor.Text);

                if (rbSwitch.Checked)
                {
                    tSwitch = Stopwatch.StartNew();
                    for (int i = 0; i < iter; i++)
                    {
                        VerSwitch(val);
                    }
                    tSwitch.Stop();
                }
                else
                {
                    if (rbIf.Checked)
                    {
                        tIf = Stopwatch.StartNew();
                        for (int i = 0; i < iter; i++)
                        {
                            VerIf(val);
                        }
                        tIf.Stop();
                    }
                    else
                    {
                        tIfElse = Stopwatch.StartNew();
                        for (int i = 0; i < iter; i++)
                        {
                            VerIfElse(val);
                        }
                        tIfElse.Stop();
                    }
                }

                if (tSwitch != null)
                {
                    lblRSwitch.Text = tSwitch.Elapsed.TotalMilliseconds.ToString();
                }
                if (tIf != null)
                {
                    lblRIf.Text = tIf.Elapsed.TotalMilliseconds.ToString();
                }
                if (tIfElse != null)
                {
                    lblRIfElse.Text = tIfElse.Elapsed.TotalMilliseconds.ToString();
                }
            }
            catch (Exception ex)
            {
                txtIter.Text = string.Empty;
                txtValor.Text = string.Empty;
                MessageBox.Show(string.Format("en iteración y valor debe ser un numero entero, ", ex.Message), "Super error...");
            }
        }

        private void btnEjecutarTodo_Click(object sender, EventArgs e)
        {
            int iter = 0, val = 0;
            Stopwatch tSwitch = null, tIf = null, tIfElse = null;

            try
            {
                iter = Convert.ToInt32(txtIter.Text);
                val = Convert.ToInt32(txtValor.Text);

                tSwitch = Stopwatch.StartNew();
                for (int i = 0; i < iter; i++)
                {
                    VerSwitch(val);
                }
                tSwitch.Stop();

                tIf = Stopwatch.StartNew();
                for (int i = 0; i < iter; i++)
                {
                    VerIf(val);
                }
                tIf.Stop();

                tIfElse = Stopwatch.StartNew();
                for (int i = 0; i < iter; i++)
                {
                    VerIfElse(val);
                }
                tIfElse.Stop();

                if (tSwitch != null)
                {
                    lblRSwitch.Text = tSwitch.Elapsed.TotalMilliseconds.ToString();
                }
                if (tIf != null)
                {
                    lblRIf.Text = tIf.Elapsed.TotalMilliseconds.ToString();
                }
                if (tIfElse != null)
                {
                    lblRIfElse.Text = tIfElse.Elapsed.TotalMilliseconds.ToString();
                }
            }
            catch (Exception ex)
            {
                txtIter.Text = string.Empty;
                txtValor.Text = string.Empty;
                MessageBox.Show(string.Format("en iteración y valor debe ser un numero entero, ", ex.Message), "Super error...");
            }
        }        
    }
}
