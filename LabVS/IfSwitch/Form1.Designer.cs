﻿namespace IfSwitch
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbSwitch = new System.Windows.Forms.RadioButton();
            this.rbIf = new System.Windows.Forms.RadioButton();
            this.rbIfElse = new System.Windows.Forms.RadioButton();
            this.lblValor = new System.Windows.Forms.Label();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.lblTSwich = new System.Windows.Forms.Label();
            this.lblRSwitch = new System.Windows.Forms.Label();
            this.lblItera = new System.Windows.Forms.Label();
            this.txtIter = new System.Windows.Forms.TextBox();
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.lblRIf = new System.Windows.Forms.Label();
            this.lblIf = new System.Windows.Forms.Label();
            this.lblRIfElse = new System.Windows.Forms.Label();
            this.lblIfElse = new System.Windows.Forms.Label();
            this.btnEjecutarTodo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rbSwitch
            // 
            this.rbSwitch.AutoSize = true;
            this.rbSwitch.Location = new System.Drawing.Point(15, 39);
            this.rbSwitch.Name = "rbSwitch";
            this.rbSwitch.Size = new System.Drawing.Size(55, 17);
            this.rbSwitch.TabIndex = 0;
            this.rbSwitch.TabStop = true;
            this.rbSwitch.Text = "switch";
            this.rbSwitch.UseVisualStyleBackColor = true;
            // 
            // rbIf
            // 
            this.rbIf.AutoSize = true;
            this.rbIf.Location = new System.Drawing.Point(90, 39);
            this.rbIf.Name = "rbIf";
            this.rbIf.Size = new System.Drawing.Size(30, 17);
            this.rbIf.TabIndex = 1;
            this.rbIf.TabStop = true;
            this.rbIf.Text = "if";
            this.rbIf.UseVisualStyleBackColor = true;
            // 
            // rbIfElse
            // 
            this.rbIfElse.AutoSize = true;
            this.rbIfElse.Location = new System.Drawing.Point(142, 39);
            this.rbIfElse.Name = "rbIfElse";
            this.rbIfElse.Size = new System.Drawing.Size(55, 17);
            this.rbIfElse.TabIndex = 2;
            this.rbIfElse.TabStop = true;
            this.rbIfElse.Text = "if_else";
            this.rbIfElse.UseVisualStyleBackColor = true;
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Location = new System.Drawing.Point(12, 9);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(33, 13);
            this.lblValor.TabIndex = 3;
            this.lblValor.Text = "valor:";
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(46, 6);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(28, 20);
            this.txtValor.TabIndex = 4;
            // 
            // lblTSwich
            // 
            this.lblTSwich.AutoSize = true;
            this.lblTSwich.Location = new System.Drawing.Point(12, 79);
            this.lblTSwich.Name = "lblTSwich";
            this.lblTSwich.Size = new System.Drawing.Size(49, 13);
            this.lblTSwich.TabIndex = 5;
            this.lblTSwich.Text = "TSwitch:";
            // 
            // lblRSwitch
            // 
            this.lblRSwitch.AutoSize = true;
            this.lblRSwitch.Location = new System.Drawing.Point(61, 79);
            this.lblRSwitch.Name = "lblRSwitch";
            this.lblRSwitch.Size = new System.Drawing.Size(13, 13);
            this.lblRSwitch.TabIndex = 6;
            this.lblRSwitch.Text = "_";
            // 
            // lblItera
            // 
            this.lblItera.AutoSize = true;
            this.lblItera.Location = new System.Drawing.Point(90, 8);
            this.lblItera.Name = "lblItera";
            this.lblItera.Size = new System.Drawing.Size(62, 13);
            this.lblItera.TabIndex = 7;
            this.lblItera.Text = "Iteraciones:";
            // 
            // txtIter
            // 
            this.txtIter.Location = new System.Drawing.Point(153, 6);
            this.txtIter.Name = "txtIter";
            this.txtIter.Size = new System.Drawing.Size(72, 20);
            this.txtIter.TabIndex = 8;
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Location = new System.Drawing.Point(12, 152);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(75, 23);
            this.btnEjecutar.TabIndex = 9;
            this.btnEjecutar.Text = "Ejecutar";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // lblRIf
            // 
            this.lblRIf.AutoSize = true;
            this.lblRIf.Location = new System.Drawing.Point(38, 104);
            this.lblRIf.Name = "lblRIf";
            this.lblRIf.Size = new System.Drawing.Size(13, 13);
            this.lblRIf.TabIndex = 11;
            this.lblRIf.Text = "_";
            // 
            // lblIf
            // 
            this.lblIf.AutoSize = true;
            this.lblIf.Location = new System.Drawing.Point(12, 104);
            this.lblIf.Name = "lblIf";
            this.lblIf.Size = new System.Drawing.Size(23, 13);
            this.lblIf.TabIndex = 10;
            this.lblIf.Text = "TIf:";
            // 
            // lblRIfElse
            // 
            this.lblRIfElse.AutoSize = true;
            this.lblRIfElse.Location = new System.Drawing.Point(55, 126);
            this.lblRIfElse.Name = "lblRIfElse";
            this.lblRIfElse.Size = new System.Drawing.Size(13, 13);
            this.lblRIfElse.TabIndex = 13;
            this.lblRIfElse.Text = "_";
            // 
            // lblIfElse
            // 
            this.lblIfElse.AutoSize = true;
            this.lblIfElse.Location = new System.Drawing.Point(12, 126);
            this.lblIfElse.Name = "lblIfElse";
            this.lblIfElse.Size = new System.Drawing.Size(43, 13);
            this.lblIfElse.TabIndex = 12;
            this.lblIfElse.Text = "TIfElse:";
            // 
            // btnEjecutarTodo
            // 
            this.btnEjecutarTodo.Location = new System.Drawing.Point(93, 152);
            this.btnEjecutarTodo.Name = "btnEjecutarTodo";
            this.btnEjecutarTodo.Size = new System.Drawing.Size(75, 23);
            this.btnEjecutarTodo.TabIndex = 14;
            this.btnEjecutarTodo.Text = "EjecutarTodo";
            this.btnEjecutarTodo.UseVisualStyleBackColor = true;
            this.btnEjecutarTodo.Click += new System.EventHandler(this.btnEjecutarTodo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 187);
            this.Controls.Add(this.btnEjecutarTodo);
            this.Controls.Add(this.lblRIfElse);
            this.Controls.Add(this.lblIfElse);
            this.Controls.Add(this.lblRIf);
            this.Controls.Add(this.lblIf);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.txtIter);
            this.Controls.Add(this.lblItera);
            this.Controls.Add(this.lblRSwitch);
            this.Controls.Add(this.lblTSwich);
            this.Controls.Add(this.txtValor);
            this.Controls.Add(this.lblValor);
            this.Controls.Add(this.rbIfElse);
            this.Controls.Add(this.rbIf);
            this.Controls.Add(this.rbSwitch);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbSwitch;
        private System.Windows.Forms.RadioButton rbIf;
        private System.Windows.Forms.RadioButton rbIfElse;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label lblTSwich;
        private System.Windows.Forms.Label lblRSwitch;
        private System.Windows.Forms.Label lblItera;
        private System.Windows.Forms.TextBox txtIter;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.Label lblRIf;
        private System.Windows.Forms.Label lblIf;
        private System.Windows.Forms.Label lblRIfElse;
        private System.Windows.Forms.Label lblIfElse;
        private System.Windows.Forms.Button btnEjecutarTodo;
    }
}

